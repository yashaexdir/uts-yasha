#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    system("clear");

    int i = 0, N = 4;  // Menyesuaikan N dengan jumlah data yang ingin dimasukkan
    float Data = 0.0, Rata = 0.0, Total = 0.0;

    // Memastikan N lebih besar dari 1 menggunakan do-while loop
    do {
        if (N <= 1) {
            cout << "Banyaknya data harus lebih besar dari 1. Silakan masukkan nilai yang valid: ";
            cin >> N;
        }
    } while (N <= 1);

    cout << "Masukan Data ke-1 : ";
    cin >> Data;
    Total = Data;  // Memasukkan data pertama ke dalam total

    for (i = 2; i <= N; i++) {
        cout << "Masukan Data ke-" << i << " : ";
        cin >> Data;
        Total += Data;
    }

    // Menghitung rata-rata
    Rata = Total / (N - 1);  // Menyesuaikan pembagi agar sesuai dengan jumlah data yang dimasukkan

    // Menampilkan hasil
    cout << "\nBanyaknya Data       : " << N - 1 << endl;
    cout << "Total Nilai Data     : " << fixed << setprecision(2) << Total << endl;
    cout << "Rata-rata nilai data : " << fixed << setprecision(2) << Rata << endl;

    return 0;
}
